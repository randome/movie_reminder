Movie Reminder
=========

MovieReminder is a (for now) chrome extension that reminds you when your saved upcoing films are comming to a cinema near you!

Features
===
- Easily add a movie to your collection from your browser
- You will recieive email notification when the saved movies are playing a cinema near you
- You will (opionally) receive notifications when the movie is available on DVD / BluRay


Setup
===
Create config.py under /indstance
Add the following keys :
TMDB_API = '123'
SECRET_KEY = '321!'
DEBUG = True
