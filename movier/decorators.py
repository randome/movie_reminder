from functools import wraps
from flask import session, Response, g

from db import db, ObjectId

def get_user(userId):
  return db.users.find_one({"_id": ObjectId(userId)})

def login_required(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    if 'userId' not in session:
      return Response('No go', 401)
    user = get_user(session['userId'])
    g.user = user
    if user == None:
      return Response('Not logged in', 403) 
    return f(*args, **kwargs)
  return decorated
