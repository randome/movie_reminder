from flask import Flask
from flaskext.bcrypt import Bcrypt

app = Flask(__name__, instance_relative_config=True)
bcrypt = Bcrypt(app)
app.config.from_pyfile('config.py')
