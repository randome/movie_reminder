from . import resources

def init(api):
	api.add_resource(resources.MovieAPI, '/api/movies/<string:id>', endpoint = 'movie')
	api.add_resource(resources.SearchMovieAPI, '/api/search/<string:title>', endpoint = 'search')
	api.add_resource(resources.MovieListApi, '/api/movies', endpoint = 'movies')
	api.add_resource(resources.UserApi, '/api/user', '/api/user/<string:userId>', endpoint = 'user')
	api.add_resource(resources.LoginApi, '/api/login', endpoint = 'login')
