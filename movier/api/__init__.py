from flask.ext.restful import Api


from .. import tmdb
from . import routes


def init(app):
	api = Api(app)
	tmdb.init(app.config['TMDB_API'])
	routes.init(api)
