from ..resources import movie_api
from ..resources import movie_list_api
from ..resources import user_api
from ..resources import login_api
from ..resources import search_api

MovieAPI = movie_api.MovieAPI
MovieListApi = movie_list_api.MovieListApi
UserApi = user_api.UserApi
LoginApi = login_api.LoginApi
SearchMovieAPI = search_api.SearchMovieAPI
