import requests
import json

headers = {"Accept": "application/json"}
params = {}
url = 'http://api.themoviedb.org/3'

def init(api_key):
	global params
	params = {'api_key': api_key}

def searchMovie(title):
	params['query'] = title
	r = requests.get(url + '/search/movie', headers=headers, params=params)
	results = r.json()['results']
	return results
