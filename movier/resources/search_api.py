from flask.ext.restful import Resource, reqparse, fields, marshal

from ..decorators import login_required
from ..tmdb import searchMovie

class SearchMovieAPI(Resource):
  decorators = [login_required]

  def __init__(self):
    self.reqparse = reqparse.RequestParser()
    self.reqparse.add_argument('title', type = str, required = True, location = 'json')
    self.movie_fields = {
      'title': fields.String,
      'original_title': fields.String,
      'release_date': fields.String
    }    
    super(SearchMovieAPI, self).__init__()

  def get(self, title):
    print title
    movies = searchMovie(title)
    #return marshal(movies, self.movie_fields)
    return movies
