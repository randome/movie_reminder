from flask import g
from flask.ext.restful import Resource, reqparse, fields, marshal

import datetime

from ..decorators import login_required
from ..db import db
from ..db import ObjectId
from ..tmdb import searchMovie

class MovieListApi(Resource):
  decorators = [login_required]

  def __init__(self):
    self.reqparse = reqparse.RequestParser()
    self.reqparse.add_argument('title', type = str, required = True, location = 'json')
    self.movie_fields = {
      '_id': fields.String,
      'title': fields.String,
      'original_title': fields.String,
      'popularity': fields.String,
      'release_date': fields.String,
      'vote_average': fields.String,
      'vote_count': fields.String,
      'imdb_id': fields.String
    }
    super(MovieListApi, self).__init__()

  def get(self):
    user = g.user
    movieList = db.movies.find({
      '_id': {'$in': user['movieList']}
      })

    return map(lambda m: marshal(m, self.movie_fields), movieList)

  def post(self):
    args = self.reqparse.parse_args()
    title = args['title']
    res = searchMovie(title)
    if len(res) > 0:
      found_movie = res[0]
    else:
      found_movie = None

    if len(res) == 0:
      return 'movie not found', 404

    del found_movie['poster_path']
    if 'backdrop_path' in found_movie:
      del found_movie['backdrop_path']

    # found_movie['release_date'] = datetime.datetime.combine(found_movie['release_date'], datetime.datetime.min.time())

    # pprint(found_movie)

    movie_id = db.movies.insert(found_movie)

    # movie_id = db.movies.insert(movie)
    # movie['_id'] = movie_id
    user = g.user
    user['movieList'].append(movie_id)
    db.users.save(user)
    # movie['movieId'] = movie_id
    return marshal(found_movie, self.movie_fields)
