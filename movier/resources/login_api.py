from flask import session
from flask.ext.restful import Resource, reqparse, fields, marshal

from ..db import db
from .. import bcrypt

class LoginApi(Resource):
  def __init__(self):
    self.reqparse = reqparse.RequestParser()
    self.reqparse.add_argument('email', type = str, required = True, location = 'json')
    self.reqparse.add_argument('password', type = str, required = True, location = 'json')
    self.user_fields = {
      '_id': fields.String,
      'email': fields.String,
      'active': fields.Boolean,
      'created': fields.DateTime
    }
    super(LoginApi, self).__init__()

  def post(self):
    args = self.reqparse.parse_args()
    user = db.users.find_one({"email": args['email']})
    if user == None:
      return 'user not found', 404
    password = user['password']
    if bcrypt.check_password_hash(password, args['password']):
      session['userId'] = str(user['_id'])
      return marshal(user, self.user_fields)
    else:
      return 'wrong credentials', 403
