from flask import g, session, abort
from flask.ext.restful import Resource, reqparse, fields, marshal
from ..decorators import login_required

from ..db import db
from ..db import ObjectId
from .. import bcrypt
import datetime

class UserApi(Resource):
  def __init__(self):
    self.reqparse = reqparse.RequestParser()
    self.reqparse.add_argument('email', type = str, required = True, location = 'json')
    self.reqparse.add_argument('password', type = str, required = True, location = 'json')
    self.user_fields = {
      '_id': fields.String,
      'email': fields.String,
      'active': fields.Boolean,
      'created': fields.DateTime
    }
    super(UserApi, self).__init__()

  def post(self):
    args = self.reqparse.parse_args()

    existing_user = db.users.find_one({"email": args['email']})
    if existing_user != None:
      abort(409, "user aready exists")

    user = {
      'email': args['email'],
      'password': bcrypt.generate_password_hash(args['password']),
      'active': True,
      'movieList': [],
      'created': datetime.datetime.utcnow()
    }

    user_id = db.users.insert(user)
    # user['userId'] = user_id

    if user_id == None:
      abort(500)

    session['userId'] = str(user_id)
    return marshal(user, self.user_fields)

  @login_required
  def get(self, userId):
    print(userId, g.user['_id'])
    if userId != str(g.user['_id']):
      return 'user not found', 404
    return marshal(g.user, self.user_fields)
