from flask.ext.restful import Resource, reqparse, fields, marshal

from ..decorators import login_required
from ..db import db
from ..db import ObjectId

class MovieAPI(Resource):
  decorators = [login_required]
  
  def __init__(self):
    self.reqparse = reqparse.RequestParser()
    self.reqparse.add_argument('title', type = str, required = True, location = 'json')
    self.movie_fields = {
      '_id': fields.String,
      'title': fields.String,
      'original_title': fields.String,
      'popularity': fields.String,
      'release_date': fields.String,
      'vote_average': fields.String,
      'vote_count': fields.String,
      'imdb_id': fields.String
    }
    super(MovieAPI, self).__init__()

  def get(self, id):
    movie = db.movies.find_one({'_id':ObjectId(id)})
    if movie == None:
      return "Movie not found", 404

    return marshal(movie, self.movie_fields)

  def put(self, id):
    return abort(501)
    ##TODO : crate updates
    args = self.reqparse.parse_args()
    movie = filter(lambda m: m['id'] == id, movies)
    if len(movie) == 0:
        return "Movie not found", 404

    movie = movie[0]
    print(movie)
    movie['title'] = args.get('title', movie['title'])
    return marshal(movie, self.movie_fields)

  def delete(self, id):
    #todo : implement this
    return "Not implemented", 501
