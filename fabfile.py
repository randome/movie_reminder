from fabric.api import *

env.user = 'dejan'
env.hosts = ['movierem.cloudapp.net']
env.path = '/home/dejan/movie_reminder'


def checkout():
    with cd(env.path):
        run('git pull')

def restart():
    sudo('supervisorctl restart gunicorn')

def deploy():
    checkout()
    restart()
