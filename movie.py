from tmdb3 import set_key, set_cache, searchMovie, Movie
from flask import Flask, jsonify
from flask.ext import restful
import json, datetime

app = Flask(__name__)
api = restful.Api(app)

def default(obj):
	if isinstance(obj, datetime.date):
		return obj.isoformat()

	if isinstance(obj, datetime.datetime):
		return obj.isoformat()

class MovieSearch(restful.Resource):
	def get(self, movie_name):
		res = searchMovie(movie_name)
		result = []
		for movie in res:
			result.append({
					'id':movie.id,
					'title':movie.title,
					'release_date':movie.releasedate,
					'releases':movie.releases
				})

		return json.dumps(result, default=default)

def init():
	set_key("3c822471a33deb9a31104e51996b3389")
	set_cache(filename='./cache.db')
	# res = searchMovie('Star Wars')
	#for movie in res:
	#	print("%s %s", movie.title, movie.release_date)

api.add_resource(MovieSearch, '/search/<string:movie_name>')

if __name__ == '__main__':
	init()
	# app.run(debug=True)
	m = Movie.fromIMDB('tt2229499')
	print m.releases
