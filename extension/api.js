'use strict';

(function () {
	var MovieReminder = this.MovieReminder = {};

	MovieReminder.VERSION = '0.1b';

	var Api = MovieReminder.Api = function (options) {
		options || (options = {});
	}

	_.extend(Api.prototype, {
		baseUrl: 'http://localhost:5000/api/',

		login: function(email, password) {
			var data = {
					email: email,
					password: password
				};

			var a = $.ajax({
				type: "POST",
				url: this.baseUrl + 'login',
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				data: JSON.stringify(data)
			});

			return a;
		},

		signup: function(email, password) {
			var data = {
					email: email,
					password: password
				};

			var a = $.ajax({
				type: "POST",
				url: this.baseUrl + 'user',
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				data: JSON.stringify(data)
			});

			return a;
		},

		getUser: function (userId) {
			var a = $.ajax({
				type: "GET",
				url: this.baseUrl + 'user/' + userId,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8'
			});

			return a;
		},

		saveMovie: function (title) {
			var data = {
				'title': title
			};

			var request = $.ajax({
				type: "POST",
				url: this.baseUrl + 'movies',
				dataType: 'json',
				data: JSON.stringify(data),
				contentType: 'application/json; charset=utf-8'
			});

			return request;
		},

		loadMovieList: function () {
			var request = $.ajax({
				type: "GET",
				url: this.baseUrl + 'movies',
				dataType: 'json',
				contentType: 'application/json; charset=utf-8'
			});

			return request;
		},
	});

}).call(this);
