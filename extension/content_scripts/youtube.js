var title = $("#eow-title");

chrome.runtime.onMessage.addListener(function (data, sender, sendResponse){
	sendResponse({
		title: title.text().trim()
	});
});
