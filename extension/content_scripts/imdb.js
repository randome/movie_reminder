var title = $("h1.header .itemprop");
var imdbID = window.location.pathname;

chrome.runtime.onMessage.addListener(function (data, sender, sendResponse){
	sendResponse({
		title: title.text(),
		imdbID: imdbID
	});
});
