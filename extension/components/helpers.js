/** @jsx React.DOM */

var Reffr = {
    ref : function(ref) {
        return this.refs[ref].getDOMNode().value;
    }
};


var LoadingButton = React.createClass({
    propTypes : function() {
        status : ReactPropTypes.oneOf(["success", "fail", "default"])
    },

    getDefaultProps: function() {
        return {
            text : "Button",
            loadingText : "Loading ...",
            successText : "Success",
            failText : "Failed"
        };
    },

    getInitialState: function() {
        return {
            disabled: false,
            text : this.props.text
        };
    },

    handleClick: function() {
        this.setState({
            text: this.props.loadingText
        });

        this.props.onClick();
    },

    render: function () {
        var isDisabled = this.props.status != "default";
        var text = this.state.text;
        var classes = ["btn", "btn-block", "btn-primary"];

        switch(this.props.status)
        {
            case "default":
                text = this.props.text;
                classes[2] = "btn-primary";
            break;
            case "success":
                text = this.props.successText;
                classes[2] = "btn-success";
            break;
            case "fail":
                text = this.props.failText;
                classes[2] = "btn-danger";
            break;
        }

        return (
            <button type="button" className={classes.join(' ')}
            onClick={this.handleClick} disabled={isDisabled}>{text}</button>
        );
    }
});
