/** @jsx React.DOM */

var PanelHeading = React.createClass({
    render: function() {
        return (
            <div className="panel-heading">
                <div className="text-center"><strong>{this.props.title}</strong></div>
            </div>
        );
    }
});

// TODO : Make it more dynamic
var PanelFooter = React.createClass({
    render: function() {
        return (
            <div className="panel-footer">
                <a href="#signUpPanel" data-toggle="csw">Sign up</a> instead or <a href="#mainPanel" data-toggle="csw">Cancel</a>
            </div>
        );
    }
});

// LOGIN PANEL

var LoginPanelBody = React.createClass({
    mixins : [Reffr],

    getInitialState: function() {
        return {
            status : "default",
            resetDelay : 850
        };
    },

    resetStatus: function() {
        this.setState(this.getInitialState());
    },

    switchToMainPanel: function () {
      $.cws("#mainPanel");
    },

    handleClick: function() {
        data = {
            email: this.ref("email"),
            password : this.ref("password")
        }

        api.login(data.email, data.password)
        .done(function() {
            this.setState({status: "success"});
            setTimeout(this.switchToMainPanel, this.state.resetDelay);
        }.bind(this))
        .fail(function() {
            this.setState({status: "fail"});
            setTimeout(this.resetStatus, this.state.resetDelay);
        }.bind(this));
    },

    render: function() {
        return (
            <div className="panel-body">
                <form role="form">
                    <div className="form-group">
                        <label for="loginEmail">Email</label>
                        <input type="text" id="loginEmail" className="form-control" placeholder="Enter email" ref="email"/>
                    </div>

                    <div className="form-group">
                        <label for="loginPassword">Password</label>
                        <input type="password" className="form-control" id="loginPassword" placeholder="Password" ref="password"/>
                    </div>

                    <LoadingButton
                        text="Login"
                        loadingText="Logging in..."
                        successText="Logged in succesfully!"
                        failText="Failed to login"
                        status={this.state.status}
                        onClick={this.switchToMainPanel}/>
                </form>
            </div>
            );
    }
});

// LOGIN PANEL

var SignUpPanelBody = React.createClass({
    mixins : [Reffr],

    getInitialState: function() {
        return {
            status : "default",
            resetDelay : 850
        };
    },

    resetStatus: function() {
        this.setState(this.getInitialState());
    },

    switchToMainPanel: function () {
      $.cws("#mainPanel");
    },

    handleClick: function() {
        data = {
            email: this.ref("email"),
            password : this.ref("password")
        }

        api.signup(data.email, data.password)
        .done(function() {
            this.setState({status: "success"});
            setTimeout(this.switchToMainPanel, this.state.resetDelay);
        }.bind(this))
        .fail(function() {
            this.setState({status: "fail"});
            setTimeout(this.resetStatus, this.state.resetDelay);
        }.bind(this));
    },

    render: function() {
        return (
            <div className="panel-body">
                <form role="form">
                    <div className="form-group">
                        <label htmlFor="Email">Email</label>
                        <input type="text" name="Email" className="form-control" placeholder="Enter email" ref="email"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="Password">Password</label>
                        <input type="password" className="form-control" name="Password" placeholder="Password" ref="password"/>
                    </div>

                    <LoadingButton
                        text="SignUp"
                        loadingText="Signing up..."
                        successText="Signed-up in succesfully!"
                        failText="Failed to SignUp"
                        status={this.state.status}
                        onClick={this.handleClick}/>
                </form>
            </div>
            );
    }
});


var AddMoviePanelBody = React.createClass({
    mixins : [Reffr],

    getInitialState: function() {
        return {
            status: 'default',
            resetDelay: 850
        }
    },

    resetStatus: function() {
        this.setState(this.getInitialState());
    },

    handleClick: function() {
        api.saveMovie(this.ref("movieTitle"))
        .done(function() {
            this.setState({status: 'success'});
            setTimeout(this.resetStatus, this.state.resetDelay);
        }.bind(this))
        .fail(function(rsp) {
            this.setState({status: 'fail'});
            setTimeout(this.resetStatus, this.state.resetDelay);
        }.bind(this));
    },

    render: function() {
        return (
            <div className="panel-body">
                <form role="form">
                    <div className="form-group">
                        <input type="text" ref="movieTitle" placeholder="Movie title" className="form-control" />
                    </div>
                    <LoadingButton
                        text="Save Movie"
                        loadingText="Saving..."
                        successText="Movie Saved!"
                        failText="Failed to save movie"
                        status={this.state.status}
                        onClick={this.handleClick}/>
                </form>
            </div>
        );
    }
});
