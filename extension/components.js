/** @jsx React.DOM */
var api = new MovieReminder.Api();


var LoginPanel = React.createClass({
    render: function() {
        return (
            <div className="tab-pane panel panel-default">
                <PanelHeading title={this.props.title}/>
                <LoginPanelBody />
                <div className="panel-footer">
                    <a href="#signUpPanel" data-toggle="csw">Sign up</a> instead
                </div>
            </div>
        );
    }
});

var SignUpPanel = React.createClass({
    render: function() {
        return (
            <div className="tab-pane panel panel-default">
                <PanelHeading title={this.props.title}/>
                <SignUpPanelBody />
                <div className="panel-footer">
                    <a href="#signUpPanel" data-toggle="csw">Login</a> instead
                </div>
            </div>
        );
    }
});

var AddMoviePanel = React.createClass({
    render: function() {
        return (
            <div className="tab-pane panel panel-default">
                <PanelHeading title={this.props.title}/>
                <AddMoviePanelBody />
                <div className="panel-footer">
                    Hi {this.props.username}
                </div>
            </div>
        );
    }
});

React.renderComponent(<LoginPanel title="Login"/>,
    document.getElementById('loginPanel'));

React.renderComponent(<SignUpPanel title="Sign up" />,
    document.getElementById('signUpPanel'));

React.renderComponent(<AddMoviePanel title="Add a movie" username="Dejan" />,
    document.getElementById('mainPanel'));

