$(function() {

	function MovieReminderApi() {
		this.username = '';
		this.api_token = '';
		this.baseUrl = 'http://localhost:5000/api/';
	}


	MovieReminderApi.prototype.login = function(username, password) {
		var a = $.ajax({
			type: "GET",
			url: "http://api.walkclap.com/api/user",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			beforeSend: function (xhr) {
				var auth = btoa(username + ":" + password);
				xhr.setRequestHeader("Authorization", "Basic " + auth);
			}
		});
		return a;
	}

	MovieReminderApi.prototype.signup = function(username, password) {
		var data = {
				username: username,
				password: password
			};

		var a = $.ajax({
			type: "POST",
			url: this.baseUrl + 'user',
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			data: JSON.stringify(data),
			success: function (user) {
				debugger
				this.api_token = user.api_token;
				this.options.username = user.username;
			}
		});

		return a;
	};

	MovieReminderApi.prototype.saveMovie = function(title) {
		var data = {
			'title': title
		};

		$.ajax({
			type: "POST",
			url: MovieReminderApi.baseUrl + 'movies',
			dataType: 'json',
			data: JSON.stringify(data),
			contentType: 'application/json; charset=utf-8',
			beforeSend: function (xhr) {
				var auth = btoa(this.username + ":" + this.api_token);
				xhr.setRequestHeader("Authorization", "Basic " + auth);
			},						
			success: function(rsp) {
				console.log(rsp)
			},
			error: function(xhr, status, msg) {
				console.log(xhr);
			}
		});		
	};

	var api = {
		options : {
			username: '',
			api_token: '',
			baseUrl: 'http://localhost:5000/api/',
		},

		login: function(username, password) {
			var a = $.ajax({
				type: "GET",
				url: "http://api.walkclap.com/api/user",
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				beforeSend: function (xhr) {
					var auth = btoa(username + ":" + password);
					xhr.setRequestHeader("Authorization", "Basic " + auth);
				}
			});
			return a;
		},

		signup: function(username, password) {
			var data = {
					username: username,
					password: password
				};

			var a = $.ajax({
				type: "POST",
				url: api.baseUrl + 'user',
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				data: JSON.stringify(data),
				success: function (user) {
					this.options.api_token = user.api_token;
					this.options.username = user.username;
				}
			});

			return a;
		},

		saveMovie: function(title) {
			var data = {
				'title': title
			};

			$.ajax({
				type: "POST",
				url: api.baseUrl + 'movies',
				dataType: 'json',
				data: JSON.stringify(data),
				contentType: 'application/json; charset=utf-8',
				beforeSend: function (xhr) {
					debugger
					var auth = btoa(api.username + ":" + api.api_token);
					xhr.setRequestHeader("Authorization", "Basic " + auth);
				},						
				success: function(rsp) {
					console.log(rsp)
				},
				error: function(xhr, status, msg) {
					console.log(xhr);
				}
			});
		}
	}

	var api2 = (function() {
		//var baseUrl = 'http://api.walkclap.com/api/';
		var baseUrl = 'http://localhost:5000/api/';

		var api_token = '';
		var username = '';

		var _api = 	{
			user: {
				login: function(username, password) {
					var a = $.ajax({
						type: "GET",
						url: "http://api.walkclap.com/api/user",
						dataType: 'json',
						contentType: 'application/json; charset=utf-8',
						beforeSend: function (xhr) {
							var auth = btoa(username + ":" + password);
							xhr.setRequestHeader("Authorization", "Basic " + auth);
						}
					});
					return a;
				},

				signup: function(username, password) {
					var data = {
							username: username,
							password: password
						};
					var a = $.ajax({
						type: "POST",
						url: api.baseUrl + 'user',
						dataType: 'json',
						contentType: 'application/json; charset=utf-8',
						data: JSON.stringify(data),
						success: function (user) {
							api.api_token = user.api_token;
							api.username = user.username;
						}
					});

					return a;
				}
			},
			movie: {
				save: function(title) {
					var data = {
						'title': title
					};

					$.ajax({
						type: "POST",
						url: baseUrl + 'movies',
						dataType: 'json',
						data: JSON.stringify(data),
						contentType: 'application/json; charset=utf-8',
						beforeSend: function (xhr) {
							debugger
							var auth = btoa(api.username + ":" + api.api_token);
							xhr.setRequestHeader("Authorization", "Basic " + auth);
						},						
						success: function(rsp) {
							console.log(rsp)
						},
						error: function(xhr, status, msg) {
							console.log(xhr);
						}
					});
				}
			}
		};

		return {
			user: _api.user,
			movie: _api.movie,
			baseUrl: baseUrl,
			username: username,
			api_token: api_token
		};
	})();


	var MovieReminder = {
		api: new MovieReminderApi(),
		userId: null,

		initialize: function() {
			var cachedUserId = localStorage.getItem('movierRemUserId');

		if (cachedUserId)
			this.userId = cachedUserId;
		},

		login: function(username, password) {
			this.api.login(username, password)
			.done(function(user) {
				// this.userId = response['_id'];
				console.log("IT WORED", user);
			})
			.fail(function(xhr, status) {
				console.log(xhr, status);
			});
		},

		signup: function(username, password) {
			this.api.signup(username, password)
			.done(function (user) {
				console.log("awesome", user);
				localStorage.setItem('movierRemUserId', user.userId);
				debugger
			})
			.fail(function (response) {
				console.log("FAILED", response);
			})
		},

		saveMovie: function (title) {
			if (title === "") {
				console.log("Title cannot be empty");
				return;
			}
			this.api.saveMovie(title);
		},

		togglePanel: function(panelId) {

		}
	};

	MovieReminder.signup("dejanpejan", "dejan");
	// MovieReminder.saveMovie("awesome movie");

	window.mr = MovieReminder; // TODO : Remove on deploy
});


