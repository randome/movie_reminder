+function($) {
	"use strict";

	var ContentSwitch = function(element) {
		this.element = $(element);
	}

	ContentSwitch.prototype.show = function() {
		var $this = this.element;
		var selector = $this.attr('href');
		var $target = $(selector);

		this.activate($target, $target.parent());
	};

	ContentSwitch.prototype.activate = function(element, container) {
		var $active = container.find('> .active');

		$active.removeClass('active');

		element.addClass('active');

		element.trigger('cws-activiate');
	};


	$.fn.csw = function(option) {
		return this.each(function() {
			var data = new ContentSwitch(this);
			data[option]();
		})
	};

	$.cws = function(switchTo) {
		var o = new ContentSwitch(switchTo);
		o.activate($(switchTo), $(switchTo).parent());
	};

	$.fn.csw.Constructor = ContentSwitch

	$(document).on('click', '[data-toggle="csw"]', function(e) {
		$(this).csw('show');
	});
}(jQuery);
