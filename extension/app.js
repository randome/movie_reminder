'use strict';

$(function() {
	var api = new MovieReminder.Api(),
		userId = localStorage.getItem('movieRemUserId'),
		saveMovieButton = $("#saveMovie"),
		signUpBtn = $("#signUpBtn"),
		loginBtn = $("#loginBtn"),
		movieTitleInput = $("#movieTitle"),
		footerText = "<a href='#movieListPanel' data-toggle='csw'>See your saved movies</a>",
		movieListPanel = $("#movieListPanel");

	if (!userId) {
		$.cws("#loginPanel");
	} else {
		// get data and verify account is active
		api.getUser(userId)
		.done(function(user) {
			console.log("found user data", user);
			$("#mainPanel .panel-footer").html(footerText);
			if (user.active == false) {
				console.error("Not active user!")
			}
			this.user = user;
		})
		.fail(function(xhr) {
			console.debug("Something went wrong retreiving a user", xhr);
			localStorage.removeItem('movieRemUserId');
		})
	}

	saveMovieButton.click(function(e) {
		e.preventDefault();
		api.saveMovie(movieTitleInput.val())
		.done(function(rsp) {
			saveMovieButton.text("Saved");
			saveMovieButton.removeClass("btn-primary");
			saveMovieButton.addClass("btn-success");
			console.log("Movie saved", rsp.movieId)
			setTimeout(function() {
				saveMovieButton.removeClass("btn-success");
				saveMovieButton.addClass("btn-primary");
				saveMovieButton.text("Save Movie");
			}, 750);
		})
		.fail(function(xhr) {
			console.error("Could not save movie", xhr);
			saveMovieButton.removeClass("btn-primary");
			saveMovieButton.addClass("btn-danger");
			saveMovieButton.text("Error saving");
			setTimeout(function() {
				saveMovieButton.removeClass("btn-danger");
				saveMovieButton.addClass("btn-primary");
				saveMovieButton.text("Save Movie");
			}, 750);


		});
	});

	loginBtn.click(function(e) {
		var btn = $(this);
		e.preventDefault();
		api.login($("#loginEmail").val(), $("#loginPassword").val())
		.done(function(user) {
			console.log("loggedin", user);
			localStorage.setItem('movieRemUserId', user._id);
			btn.removeClass("btn-primary");
			btn.addClass("btn-success");
			btn.text("Success");
			setTimeout(function() {
				$.cws("#mainPanel");
				$("#mainPanel .panel-footer").html(footerText);
			}, 1000);
		})
		.fail(function(xhr) {
			console.debug("Login failed", xhr);
			btn.removeClass("btn-primary");
			btn.addClass("btn-danger");
			btn.text("Login failed");
			setTimeout(function() {
				btn.removeClass("btn-danger");
				btn.addClass("btn-primary");
				btn.text("Login");
			}, 1550);
		})
	});

	signUpBtn.click(function(e) {
		var btn = $(this);
		e.preventDefault();
		api.signup($("#signUpEmail").val(), $("#signUpPassword").val())
		.done(function(user) {
			localStorage.setItem('movieRemUserId', user._id);
			btn.removeClass("btn-primary");
			btn.text("Done");
			btn.addClass("btn-success");
			$("#mainPanel .panel-footer").html(footerText);
			setTimeout(function() {
				$.cws('#mainPanel');
			}, 750)
		})
		.fail(function(xhr) {
			console.error("Could not sign up", xhr);
			btn.removeClass("btn-primary");
			btn.addClass("btn-danger");
			btn.text("Signup failed");
			setTimeout(function() {
				btn.removeClass("btn-danger");
				btn.addClass("btn-primary");
				btn.text("Sign up");
			}, 1550);
		});
	});

	movieListPanel.on('cws-activiate', function(e) {
		var listGroup = $("#movieListPanel .list-group");
		api.loadMovieList()
		.done(function(movieList){
			var items = [];

			if (movieList.length == 0) items.push("No saved movies found. Save a movie first!");

			_.each(movieList, function(movie){
				console.log(movie.title);
				items.push('<li class="list-group-item">' + movie.title + '</li>')
			});
			listGroup.html(items.join(''));
		})
		.fail(function(xhr){
			console.error("Could not load movies", xhr);
		});
	});
});
